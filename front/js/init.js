(function($){
  	$(function(){

    	$('.button-collapse').sideNav({menuWidth: 240, activationWidth: 70});
  		$('.slider').slider({full_width: true});
  		$(".dropdown-button").dropdown();
  		$('.collapsible').collapsible({
	      accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
	    });

  	}); // end of document ready
})(jQuery); // end of jQuery name space